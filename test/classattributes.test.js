import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/attributes/classattributes.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    Classes: ['foo', 'bar', 'baz'],
    CurrentUrl: '/about'
  })).toMatchSnapshot()
})
