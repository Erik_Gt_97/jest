import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/attributes/classandidliteral.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    Frase: 'Do it'
  })).toMatchSnapshot()
})
