import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/interpolacionjs.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    user: {
      name: 'Leeroy Jenkins',
      id: 1234567890,
      address: '123 Wilson Way, New York NY, 10165'
    }
  })).toMatchSnapshot()
})
