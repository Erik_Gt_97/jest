import callbackexample from '../src/callbackexample'

test('Probando un callback', done => {
  function callback (data) {
    expect(data).toBe('peanut butter')
    done()
  }

  callbackexample('peanut butter', callback)
}, 10000)
