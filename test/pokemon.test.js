import Pokemon from '../src/api/pokemon'
const a = new Pokemon('ditto')
test('plays video', () => {
  const spy = jest.spyOn(a, 'volume', 'set')
  a.volume = 'test'
  expect(spy).toHaveBeenCalled()

  // expect(test).toBe('ditto')
  // expect(a.NamePokemon).toBe('imposter')
  expect(a._nameability).toBe('test')
  a.NamePokemon
    .then(data => {
      expect(data.abilities[0].ability.name).toBe('imposter')
    })

  spy.mockRestore()
})
