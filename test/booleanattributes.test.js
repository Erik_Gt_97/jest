import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/attributes/booleanattributes.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    True: true,
    False: false
  })).toMatchSnapshot()
})
