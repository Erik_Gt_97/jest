import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/interpolacionvariablessinprocesar.pug', data)

test('It should render pug correctly', () => {
  const tag = "<div>You can't escape me!</div>"
  expect(renderPug({
    myTag: tag
  })).toMatchSnapshot()
})
