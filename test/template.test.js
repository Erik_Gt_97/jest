import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/template.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    myTitle: 'Pug',
    myText: 'Pug is great'
  })).toMatchSnapshot()
})
