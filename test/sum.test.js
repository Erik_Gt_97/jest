import sum from '../src/sum'

describe('Realizando la suma de 2 numeros', () => {
  it('expect fun', () => {
    expect(sum(8, 10)).toBe(18)
    expect(sum(1, 2)).toBeGreaterThan(2)
    expect(sum(0.1, 0.2)).not.toBe(0.3)
    expect(sum(10, -15)).toBeLessThanOrEqual(0)
    expect(sum(0.1, 0.4)).toBeCloseTo(0.5)
  })
})
