import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/attributes/attributes.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    bool: true
  })).toMatchSnapshot()
})
