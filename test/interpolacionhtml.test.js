import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/interpolacionhtml.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    Title: 'Interpolacion HTML'
  })).toMatchSnapshot()
})
