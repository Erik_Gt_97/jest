import promesa from '../src/promesa'
test('the data is peanut butter', () => {
  expect.assertions(1)
  return promesa().then(data => {
    expect(data).toBe('peanut butter')
  })
})
