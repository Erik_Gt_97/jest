import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/helloworld.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    name: 'Erik'
  })).toMatchSnapshot()
})
