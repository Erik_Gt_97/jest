import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/each.pug', data)

test('It should render pug correctly', () => {
  const bookStore = [
    {
      title: 'Templating with Pug',
      author: 'Winston Smith',
      pages: 143,
      year: 2017
    },
    {
      title: 'Node.js will help',
      author: 'Guy Fake',
      pages: 879,
      year: 2015
    }
  ]

  expect(renderPug({
    bookStore: bookStore
  })).toMatchSnapshot()
})
