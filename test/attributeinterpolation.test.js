import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/attributes/attributeinterpolation.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    info: 'info',
    size: 'lg',
    url1: 'pug-test.html',
    url2: 'https://example.com/'
  })).toMatchSnapshot()
})
