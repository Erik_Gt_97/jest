import excepcionexample from '../src/excepcionexample'
describe('Realizando una excepcion', () => {
  it('expect fun', () => {
    expect(excepcionexample).toThrow()
    expect(excepcionexample).toThrow(Error)
    // Se puede usar el tiempo exacto de error o un regex
    expect(excepcionexample).toThrow('usted usa el JDK erroneo')
    expect(excepcionexample).toThrow(/JDK/)
  })
})
