import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/interpolacionvariables.pug', data)

test('It should render pug correctly', () => {
  const colors = ['Red', 'Green', 'Blue']
  const langs = ['Html', 'CSS', 'JS']
  const title = 'My Cool Website'
  expect(renderPug({
    title: title,
    siteColors: colors,
    siteLangs: langs

  })).toMatchSnapshot()
})
