import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/ejemplo.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    pageTitle: 'Pug',
    youAreUsingPug: true
  })).toMatchSnapshot()
})
