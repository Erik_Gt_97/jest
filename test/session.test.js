// import SessionController from '../../../resources/js/Controllers/SessionController'
import pug from 'pug'

const compiledFunc = pug.compileFile('./src/resources/login.pug')

describe('Login', () => {
  beforeEach(() => {
    // document.documentElement.insertAdjacentElement('afterbegin', compiledFunc())
    document.documentElement.innerHTML = compiledFunc()
  })
  afterEach(() => {
    // restore the original func after test
    jest.resetModules()
  })

  it('SessionController: message', () => {
    // const sessionController = new SessionController()
    // sessionController.message('Lorem Ipsum')
    const elemMessage = document.getElementById('errorFlash')
    expect(elemMessage.innerHTML).toBe('')
  })

  // it('LoginForm: onSubmit', done => {
  //   const loginForm = new LoginForm()
  //   const elemUsername = document.getElementById('username')
  //   const elemPassword = document.getElementById('password')
  //   const elemBtn = document.getElementById('btn-submit')

  //   loginForm.onSubmit = form => {
  //     expect(form).toMatchObject({
  //       username: 'Juan',
  //       password: '123456'
  //     })
  //     done()
  //   }

  //   elemUsername.value = 'Juan'
  //   elemPassword.value = '123456'
  //   console.log(compiledFunc())
  //   elemBtn.click()
  // })
})
