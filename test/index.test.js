import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/index.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    Title: 'My Site'
  })).toMatchSnapshot()
})
