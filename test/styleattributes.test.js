import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/attributes/styleattributes.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    Color: 'red',
    Background: 'green'
  })).toMatchSnapshot()
})
