import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/if-else.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    bool: false
  })).toMatchSnapshot()
})
