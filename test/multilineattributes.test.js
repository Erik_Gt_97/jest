import pug from 'pug'

const renderPug = data => pug.renderFile('./src/resources/attributes/multilineattributes.pug', data)

test('It should render pug correctly', () => {
  expect(renderPug({
    bool: true
  })).toMatchSnapshot()
})
