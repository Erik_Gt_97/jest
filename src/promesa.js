function promesa () {
  const promesa = new Promise((resolve, reject) => {
    resolve('peanut butter')
  })

  return promesa
}

export default promesa
