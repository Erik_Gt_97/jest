const Axios = require('axios')

const a = new Axios({
  method: 'get',
  url: 'https://pokeapi.co/api/v2/pokemon/ditto',
  responseType: 'stream'
})

export default a
