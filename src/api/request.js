import request from './request'

function getNamePokemon (IDpokemon) {
  return request('https://pokeapi.co/api/v2/pokemon-form/' + IDpokemon + '/').then(data => data.name)
}

export default getNamePokemon
