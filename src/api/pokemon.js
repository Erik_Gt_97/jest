const axios = require('axios')
class Pokemon {
  _nameability = 'hola'
  constructor (namea) {
    this.name = namea
  }

  get NamePokemon () {
    return axios.get('https://pokeapi.co/api/v2/pokemon/' + this.name)
  }

  set volume (value) {
    this._nameability = value
  }

  get volume () {
    return this._nameability
  }
}

export default Pokemon
